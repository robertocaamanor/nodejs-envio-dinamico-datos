var express = require('express');
var bodyParser = require('body-parser');
const config = require('./config/config');
var app = express();
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
const simplifiedHystrixjs = require('simplified-hystrixjs');
const puerto = config.app.port;
var mongoose = require('mongoose');

var Discos = require('./models/discos');

mongoose.connect('mongodb://localhost:27017/disqueria', function (err) {

    if (err) throw err;

    console.log('Successfully connected');

});

app.post('/api/v1/discos/add', function (req, res) {
    let newReg = new Discos();
    // newReg.titulo = req.body.titulo;
    // newReg.artista = req.body.artista;
    // newReg.anio = req.body.anio;
    // newReg.genero = req.body.genero;
    // console.log(newReg);

    // console.log(req.body);
    var coincidio = false;
    for(var key1 in req.body){
        for ( var key2 in newReg ) {
            if(key1 == key2){
                // console.log(req.body[key1]);
                newReg[key1] = req.body[key1];
                coincidio = true;
            }
        }
    }
    if(coincidio==true){
        newReg.save(err => {
            if(err){
                return res.send(err);
            }
            res.json({message: 'Registro guardado correctamente'});
        });
    }
    // var coinciden = false;


    // if(reqBody == reg){
    //     console.log('coinciden');
    // }

    // res.json({message: 'Prueba'});



});

// ------------------ Servidor Configuracion --------------------------------------------
var server = app.listen(puerto, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('Listening at http://%s:%s', host, port);
    console.log('test at http://%s:%s', config.app.port);
});
module.exports = app;
