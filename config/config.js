const env = process.env.NODE_ENV || 'local';
const local = {
    app: {
        port: parseInt(process.env.APP_PORT) || 5121,
        host: process.env.HOST || 'localhost',
    }
};

const config = {
    local
};

module.exports = config[env];
