var mongoose = require('mongoose');
let Schema = mongoose.Schema;

let discosSchema = new Schema({
    titulo: String,
    artista: String,
    anio: String,
    genero: String
});

module.exports = mongoose.model('Discos', discosSchema);
